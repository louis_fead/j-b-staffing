<?php include("includes/overall/header.php"); ?>
	<title>J &amp; B Staffing::Contact Us</title>
	<?php include("includes/head.php"); ?>
	<link href="stylesheet.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	form {margin: 0 auto; width: 600px;}
	fieldset {width:608px;}
	form ul {margin:0; padding:0; list-style:none;}
	form li {margin-bottom:10px; overflow:hidden;}
	form label {display:block; margin-bottom:2px;}
	form label span {color:#999;}
	form .input {width:592px; border:1px solid #E0E0E0; background:#F6F6F6;}
	form select.input {border:1px solid #E0E0E0; background:#F6F6F6;}
	form .third {float:left; width:193px; margin-right:10px;}
	form .third .input {width:185px;}
	form .half {float:left; width:294px; margin-right:10px;}
	form .half .input {width:286px;}
	form .half select.input {width:294px;}
	form .omega {margin-right:0;}
	form .submit {text-align: center; width: 50%; margin: 0 auto;}
	</style>
</head>
<body>
<?php include("includes/header.php"); ?>

<?php include("includes/menu.php"); ?>

<section></section>

<div>
<h1>Contact Us</h1>
We are located at 133 W Michigan Ave Ypsilanti, MI 48197

<?php include("includes/forms/contactForm.php"); ?>

<?php include("includes/overall/footer.php"); ?>