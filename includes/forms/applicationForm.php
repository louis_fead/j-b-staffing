<form action="http://localhost/jandbstafiing_com/newapplication.php" method="post">
	<ul>
		<fieldset><legend>Personal Info</legend>
		<li>
			<div class="half">
				<label for="firstName">First Name:</label>
				<input type="text" id="firstName" name="firstName" class="input" />
			</div>
			<div class="half">
				<label for="lastName">Last Name:</label>
				<input type="text" id="lastName" name="lastName" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
				<label for="ssn"> Social Security Number (SSN): </label>
				<input type="text" id="ssn" name="ssn" class="input" />
			</div>
			<div class="half">
				<label for="phone"> Main Phone: </label>
				<input type="text" id="phone" name="phone" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
				<label for="email">Email:</label>
				<input type="text" id="email" name="email" class="input" />
			</div>
			<div class="half">
				<label for="phoneAlt"> Secondary Phone:</label>
				<input type="text" id="phoneAlt" name="phoneAlt" class="input" />
			</div>
		</li>
		<li>
			<label for="street">Street:</label>
			<input type="text" id="street" name="street" class="input" />
		</li>
		<li>
			<div class="third">
				<label for="city">City:</label>
				<input type="text" id="city" name="city" class="input" />
			</div>
			<div class="third">
				<label for="state">State:</label>
				<select name="state" size="1" class="input" >
					<option selected disabled>SELECT STATE</option>
					<optgroup label="American States & Territories">
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District Of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
					</optgroup>
					<optgroup label="US Outlying Territories">
					<option value="AS">American Samoa</option>
					<option value="GU">Guam</option>
					<option value="MP">Northern Mariana Islands</option>
					<option value="PR">Puerto Rico</option>
					<option value="UM">US Minor Outlying Islands</option>
					<option value="VI">Virgin Islands</option>
					</optgroup>
					<optgroup label="Armed Forces">
					<option value="AA">Armed Forces Americas</option>
					<option value="AP">Armed Forces Pacific</option>
					<option value="AE">Armed Forces Others</option>
					</optgroup>& Territories"
					<optgroup label="Canadian Provinces & Territories">
					<option value="AB">Alberta</option>
					<option value="BC">British Columbia</option>
					<option value="MB">Manitoba</option>
					<option value="NB">New Brunswick</option>
					<option value="NL">Newfoundland and Labrador</option>
					<option value="NS">Nova Scotia</option>
					<option value="ON">Ontario</option>
					<option value="PE">Prince Edward Island</option>
					<option value="QC">Quebec</option>
					<option value="SK">Saskatchewan</option>
					<option value="NT">Northwest Territories</option>
					<option value="NU">Nunavut</option>
					<option value="YT">Yukon</option>
					</optgroup>
					<optgroup label="Mexican States">
					<option value="AGS">Aguascalientes</option>
					<option value="BCN">Baja California</option>
					<option value="BCS">Baja California Sur</option>
					<option value="CAM">Campeche</option>
					<option value="CHP">Chiapas</option>
					<option value="CHI">Chihuahua</option>
					<option value="COA">Coahuila</option>
					<option value="COL">Colima</option>
					<option value="DIF">Distrito Federal</option>
					<option value="DUR">Durango</option>
					<option value="MEX">Estado de M&eacute;xico</option>
					<option value="GTO">Guanajuato</option>
					<option value="GRO">Guerrero</option>
					<option value="HGO">Hidalgo</option>
					<option value="JAL">Jalisco</option>
					<option value="MIC">Michoac&aacute;n</option>
					<option value="MOR">Morelos</option>
					<option value="NAY">Nayarit</option>
					<option value="NLE">Nuevo Le&oacute;n</option>
					<option value="OAX">Oaxaca</option>
					<option value="PUE">Puebla</option>
					<option value="QUE">Quer&eacute;taro</option>
					<option value="ROO">Quintana Roo</option>
					<option value="SLP">San Luis Potos&iacute;</option>
					<option value="SIN">Sinaloa</option>
					<option value="SON">Sonora</option>
					<option value="TAB">Tabasco</option>
					<option value="TAM">Tamaulipas</option>
					<option value="TLA">Tlaxcala</option>
					<option value="VER">Veracruz</option>
					<option value="YUC">Yucat&aacute;n</option>
					<option value="ZAC">Zacatecas</option>
					</optgroup>
				</select>
			</div>
			<div class="third omega">
				<label for="zip">Zip:</label>
				<input type="text" id="zip" name="zip" class="input" />
			</div>
		</li>
		</fieldset>
		<fieldset><legend>In Case Of Emergency Please Notify</legend>
		<li>
			<div class="half">
			<label for="ICE_Name">Emergency Contact Name</label>
			<input name="ICE_Name" type="text" class="input"/>
			</div>
			<div class="half">
			<label for="ICE_Phone">Phone</label>
			<input name="ICE_Phone" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div>
				<label for="ICE_Address">Address:</label>
				<input name="ICE_Address" type="text" class="input" />
			</div>
		</li>
		</fieldset>
		<fieldset><legend>Employment Overview</legend>
		<li>
			<div>
				<label for="PrevEmployedHere">Have You Ever Been Employed With J & B Staffing?</label>
				<input name="PrevEmployedHere" type="radio" value="Yes" /> Yes 
				<input name="PrevEmployedHere" type="radio" value="No" checked="checked" /> No
			</div>
		</li>
		<li>
			<div>
				<label for="JBCompaniesTemp">What Companies Did You Work For? </label>
				<textarea name="JBCompaniesTemp" rows="1" cols="19" style="width: 220px" /></textarea>
			</div>
		</li>
		<li>
			<div>
				<label for="Position">Position you are applying for *</label>
				<input name="Position" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="JB_Referred">How Were You Referred To J & B Staffing?</label>
				<input name="JB_Referred" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Days"><b>Which Days Are You Available To Work?</b></label></td><td>
				<input name="Availability_Days" type="checkbox" value="Monday" /> Monday <br />  
				<input name="Availability_Days" type="checkbox" value="Tuesday" /> Tuesday <br />  
				<input name="Availability_Days" type="checkbox" value="Wednesday" /> Wednesday <br />  
				<input name="Availability_Days" type="checkbox" value="Thursday" /> Thursday <br />  
				<input name="Availability_Days" type="checkbox" value="Friday" /> Friday <br />  
				<input name="Availability_Days" type="checkbox" value="Saturday" /> Saturday <br />  
				<input name="Availability_Days" type="checkbox" value="Sunday" /> Sunday <br />  
			</div>
		</li>
		<li>
			<div>
			<label for="Availability_Cities"><b>Cities You Are Available To Work In:</b></label>
			<input name="Availability_Cities" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Shifts"><b>Shifts Available:</b></label></td>
				<input name="Availability_Shifts" type="checkbox" value="1st" /> 1st  
				<input name="Availability_Shifts" type="checkbox" value="2nd" /> 2nd   
				<input name="Availability_Shifts" type="checkbox" value="3rd" /> 3rd 
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Start"><b>When are you available to start?</b></label>
				<input name="Availability_Start" type="date" />
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Other"><b>Please check any of the following that apply to you:</b></label>
				<input name="Availability_Other" type="checkbox" value="Longterm" /> Available for long term assignments <br />  
				<input name="Availability_Other" type="checkbox" value="Same_Day" /> Will accept same day assignments <br />  
				<input name="Availability_Other" type="checkbox" value="Temp2Perm" /> Available for temporary to permanent assignments <br /> 
				<input name="Availability_Other" type="checkbox" value="Car_Available" /> Car available <label for="OwnCar"> <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If yes, is this your own car? </label> 
				<input name="OwnCar" type="radio" value="Yes"/> Yes
				<input name="OwnCar" type="radio" value="No" /> No <br />
				<input name="Availability_Other" type="checkbox" value="Resume" /> Resume attached <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<label for="Resume" ><input name="Resume" type="file" /><br />
			</div>
		</li>
		</fieldset>
		<fieldset><legend>FIELDNAME</legend>
		<li>
			<div>
				<label for="Work_Eligiblity"><b>Are you legally eligible to work in the U.S?</b></label>
				<input name="Work_Eligiblity" type="radio" value="Yes" /> Yes      
				<input name="Work_Eligiblity" type="radio" value="No" checked="checked" /> No
			</div>
		</li>
		<li>
			<div>
			<label for="Majority"><b>Are you at least 18 years old?</b></label>
			<input name="Majority" type="radio" value="Yes" /> Yes      
			<input name="Majority" type="radio" value="No" checked="checked" /> No
			</div>
		</li>
		<li>
			<div>
			<label for="Felony"><b>Have You Ever Been Convicted Of A Felony?</b></label>
			<input name="Felony" type="radio" value="Yes" /> Yes      
			<input name="Felony" type="radio" value="No" /> No
			</div>
		</li>
		<li>
			<div>
			<label for="Felony_Explaination"><b>If Yes, Please Explain</b></label><br />
			<textarea name="Felony_Explaination" rows="7" cols="40" class="input"></textarea>
			</div>
		</li>
		</fieldset>
		<fieldset><legend>Education Background</legend>
		
		<table border="0" cellpadding="5" cellspacing="0">
		<tr> <td colspan="4"style="text-align: right;">Dates of Graduation</td></tr>
		<tr> <td colspan="2"><label for="High_School">High School Attended</label></td>
		<td><input name="High_School" type="text" maxlength="50" style="width: 295px;" class="table" /></td>
		<td><label for="High_School_GraduationDate"></label><input name="High_School_GraduationDate" type="date" class="table" /></td>
		<tr> <td colspan="2"><label for="College">College Attended</label></td>
		<td><input name="College" type="text" maxlength="50" style="width: 295px;" class="table"/></td>
		<td><label for="College_GraduationDate"></label><input name="College_GraduationDate" type="date" class="table" /></td> </tr>
		</table>
		<br />
		<li>
			<div>
			<label for="Degree_Emphasis">Degree/Emphasis</label><input name="Degree_Emphasis" type="text" class="input" />
			</div>
		</li>
		<li>
			<div>
			<label for="Special_Skills">Special Skills</label><input name="Special_Skills" type="text" class="input" />
			</div>
		</li>
		</fieldset>
		<fieldset><legend>Previous Employment History</legend>
			<!--	1st Employer	-->	
		<li>
			<div>
				<label for="1stEmployer_Name">Employer Name</label>
				<input name="1stEmployer_Name" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div>
				<label for="1stEmployer_Address">Address</label>
				<input name="1stEmployer_Address" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div class="half">
				<label for="1stEmployer_Supervisor">Name of Supervisor</label>
				<input name="1stEmployer_Supervisor" type="text" class="input"/>
			</div>
			<div class="half">
				<label for="1stEmployer_Phone">Phone Number</label>
				<input name="1stEmployer_Phone" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div class="half">
				<label for="1stEmployer_PayRate">Pay Rate</label>
				<input name="1stEmployer_PayRate" type="text" class="input" />
			</div>
			<div class="half">
				<label for="1stEmployer_DatesEmployed">Dates Employed</label>
				<input name="1stEmployer_DatesEmployed" type="text" class="input" />
			</div>
		</li>
		<li>
			<div>
			<label for="1stEmployer_Job">Job Title and Duties</label>
			<textarea name="1stEmployer_Job" rows="7" cols="40" class="input"></textarea>
			</div>
		</li>
		<li>
			<div>
				<label for="1stEmployer_Reason">Reason For Leaving</label>
				<input name="1stEmployer_Reason" type="text" class="input"/>
			</div>
		</li> 
		
		<br/>
			<!--	2nd Employer	-->	
		
		<li>
			<div>
				<label for="2ndEmployer_Name">Employer Name</label>
				<input name="2ndEmployer_Name" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div>
				<label for="2ndEmployer_Address">Address</label>
				<input name="2ndEmployer_Address" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div class="half">
				<label for="2ndEmployer_Supervisor">Name of Supervisor</label>
				<input name="2ndEmployer_Supervisor" type="text" class="input"/>
			</div>
			<div class="half">
				<label for="2ndEmployer_Phone">Phone Number</label>
				<input name="2ndEmployer_Phone" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div class="half">
				<label for="2ndEmployer_PayRate">Pay Rate</label>
				<input name="2ndEmployer_PayRate" type="text" class="input" />
			</div>
			<div class="half">
				<label for="2ndEmployer_DatesEmployed">Dates Employed</label>
				<input name="2ndEmployer_DatesEmployed" type="text" class="input" />
			</div>
		</li>
		<li>
			<div>
			<label for="2ndEmployer_Job">Job Title and Duties</label>
			<textarea name="2ndEmployer_Job" rows="7" cols="40" class="input"></textarea>
			</div>
		</li>
		<li>
			<div>
				<label for="2ndEmployer_Reason">Reason For Leaving</label>
				<input name="2ndEmployer_Reason" type="text" class="input"/>
			</div>
		</li> 
				
		<br/>
		<!--	3rd Employer	-->	
		
		<li>
			<div>
				<label for="3rdEmployer_Name">Employer Name</label>
				<input name="3rdEmployer_Name" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div>
				<label for="3rdEmployer_Address">Address</label>
				<input name="3rdEmployer_Address" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div class="half">
				<label for="3rdEmployer_Supervisor">Name of Supervisor</label>
				<input name="3rdEmployer_Supervisor" type="text" class="input"/>
			</div>
			<div class="half">
				<label for="3rdEmployer_Phone">Phone Number</label>
				<input name="3rdEmployer_Phone" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div class="half">
				<label for="3rdEmployer_PayRate">Pay Rate</label>
				<input name="3rdEmployer_PayRate" type="text" class="input" />
			</div>
			<div class="half">
				<label for="3rdEmployer_DatesEmployed">Dates Employed</label>
				<input name="3rdEmployer_DatesEmployed" type="text" class="input" />
			</div>
		</li>
		<li>
			<div>
			<label for="3rdEmployer_Job">Job Title and Duties</label>
			<textarea name="3rdEmployer_Job" rows="7" cols="40" class="input"></textarea>
			</div>
		</li>
		<li>
			<div>
				<label for="3rdEmployer_Reason">Reason For Leaving</label>
				<input name="3rdEmployer_Reason" type="text" class="input"/>
			</div>
		</li> 
		
		<br/>
		<!--	4th Employer	-->		
		
		<li>
			<div>
				<label for="4thEmployer_Name">Employer Name</label>
				<input name="4thEmployer_Name" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div>
				<label for="4thEmployer_Address">Address</label>
				<input name="4thEmployer_Address" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div class="half">
				<label for="4thEmployer_Supervisor">Name of Supervisor</label>
				<input name="4thEmployer_Supervisor" type="text" class="input"/>
			</div>
			<div class="half">
				<label for="4thEmployer_Phone">Phone Number</label>
				<input name="4thEmployer_Phone" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div class="half">
				<label for="4thEmployer_PayRate">Pay Rate</label>
				<input name="4thEmployer_PayRate" type="text" class="input" />
			</div>
			<div class="half">
				<label for="4thEmployer_DatesEmployed">Dates Employed</label>
				<input name="4thEmployer_DatesEmployed" type="text" class="input" />
			</div>
		</li>
		<li>
			<div>
			<label for="4thEmployer_Job">Job Title and Duties</label>
			<textarea name="4thEmployer_Job" rows="7" cols="40" class="input"></textarea>
			</div>
		</li>
		<li>
			<div>
				<label for="4thEmployer_Reason">Reason For Leaving</label>
				<input name="4thEmployer_Reason" type="text" class="input"/>
			</div>
		</li> 
		
		</fieldset>
		<fieldset><legend>References</legend>
		<!--	1st Reference	-->		
		<li>
			<div>
			<label for="1stReference_Name">Name</label>
			<input name="1stReference_Name" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div>
			<label for="1stReference_Address">Address</label>
			<input name="1stReference_Address" type="text" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
			<label for="1stReference_Phone">Phone Number</label>
			<input name="1stReference_Phone" type="text" class="input" /></td> 
			</div>
			<div class="half">
			<label for="1stReference_Relation">Relationship</label>
			<input name="1stReference_Relation" type="text" class="input" />
			</div>
		</li>
		
		<br/>
		<!--	2nd Reference	-->		
		<li>
			<div>
			<label for="2ndReference_Name">Name</label>
			<input name="2ndReference_Name" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div>
			<label for="2ndReference_Address">Address</label>
			<input name="2ndReference_Address" type="text" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
			<label for="2ndReference_Phone">Phone Number</label>
			<input name="2ndReference_Phone" type="text" class="input" /></td> 
			</div>
			<div class="half">
			<label for="2ndReference_Relation">Relationship</label>
			<input name="2ndReference_Relation" type="text" class="input" />
			</div>
		</li>
		</fieldset>
		
		<fieldset><legend>Temporary Services</legend>
		<!--	1st Temporary Service	-->	
		<li>	
			<div>
			<label for="1stService_Name">Name of Service</label>
			<input name="1stService_Name" type="text" class="input" />
			</div>
		</li>
		<li>
			<div>
			<label for="1stService_Assigned_Companies">Name of Companies Assigned To</label>
			<textarea name="1stService_Assigned_Companies" rows="7" cols="40" class="input"></textarea>
			</div>
		</li>
		
		<br/>
		<!--	2nd Temporary Service	-->	
		
		<li>
			<div>
			<label for="2ndService_Name">Name of Service</label>
			<input name="2ndService_Name" type="text" class="input" />
			</div>
		</li>
		<li>
			<div>
			<label for="2ndService_Assigned_Companies">Name of Companies Assigned To</label>
			<textarea name="2ndService_Assigned_Companies" rows="7" cols="40" class="input"></textarea>
			</div>
		</li>
		
		<br/>
		<!--	3rd Temporary Service	-->	
		
		<li>
			<div>
			<label for="3rdService_Name">Name of Service</label>
			<input name="3rdService_Name" type="text" class="input" />
			</div>
		</li>
		<li>
			<div>
			<label for="3rdService_Assigned_Companies">Name of Companies Assigned To</label>
			<textarea name="3rdService_Assigned_Companies" rows="7" cols="40" class="input"></textarea>
			</div>
		</li>
		</fieldset>
		
		<fieldset><legend>CERTIFICATION & SUMMITTION</legend>
		<li>
			<div>
			PLEASE READ CERTIFICATION THOROUGHLY:
			</div>
		</li>
		<li>
			<div>
			If employed, I agree that if at any time I shall make claims against the company for personal injuries, upon written request I will submit myself to examination by a physician or physicians of the company&rsquo;s selection (at the company&rsquo;s expense) as often as may be requested. I certify that the facts contained in this application and interview are true and complete. I understand that, if employed, falsified statements shall be grounds for dismissal. I understand and agree that, if hired, my employment is for no definite period and may be terminated at any time without prior notice. I authorize J&B Staffing to contact my previous employers and educational institutions regarding my employment and education.
			</div>
		</li>
		<br /><br />
		<li>
			<div>
			<input type="checkbox" name="readcert" required>I HAVE CAREFULLY READ THE ABOVE CERTIFICATION AND I UNDERSTAND AND AGREE TO ITS TERMS.
			</div>
		</li>
		<br /><br />
		<li>
			<div class="half">
			<label for="Signature">Name</label><input name="Signature" type="text" class="input" />
			</div>
		
			<div class="half">
			<label for="Signature_Date">Date</label><input name="Signature_Date" type="date" class="input" />
			</div>
		</li>
<!--		
		</fieldset>
		<fieldset><legend>FIELDNAME</legend>
		<li>
			<div>
			</div>
		</li>
		<br/>
		
		<div class="button">
        <button type="submit">Send your message</button>
    </div>
-->	
		<li>
			<div class="submit">
				<input type="submit" value="Submit">
			</div>
		</li>
	</ul>
	</fieldset>
	
	
</form>