<form action="http://localhost/jandbstafiing_com/recordadded.php" method="post">
	<b>Add New Record</b>
	<fieldset><legend>Personal Info</legend>
	<div> 
		<label class="field" for="firstName"> First Name: </label>
		<input type="text" name="firstName" size="30" value="" /> 
	</div>
	<div>
		<label class="field" for="lastName"> Last Name: </label>
		<input type="text" name="lastName" size="30" value="" />
	</div>
	<div>
		<label class="field" for="ssn_itin"> Social Security Number (SSN): </label>
		<input name="ssn" type="text" size="11" maxlength="11" value="" >
	</div>
	<div>
		<label class="field" for="phone"> Main Phone: </label>
		<input type="text" name="phone" size="15" value="" />
	</div>
	<div>
		<label class="field" for="phoneAlt"> Secondary Phone:</label>
		<input type="text" name="phoneAlt" size="15" value="" />
	</div>
	<div>
		<label class="field" for="email"> Email: </label>
		<input type="text" name="email" size="30" value="" />
	</div>
	<div>
		<label class="field" for="street"> Street: </label>
		<input type="text" name="street" size="30" value="" />
	</div>
	<div>
		<label class="field" for="city"> City: </label>
		<input type="text" name="city" size="30" value="" />
	</div>
	<div>
		<label class="field" for="state">State: </label>
			<select name="state" size="1" >
				<option selected disabled>SELECT STATE/PROVINCE</option>
				<optgroup label="American States & Territories">
				<option value="AL">Alabama</option>
				<option value="AK">Alaska</option>
				<option value="AZ">Arizona</option>
				<option value="AR">Arkansas</option>
				<option value="CA">California</option>
				<option value="CO">Colorado</option>
				<option value="CT">Connecticut</option>
				<option value="DE">Delaware</option>
				<option value="DC">District Of Columbia</option>
				<option value="FL">Florida</option>
				<option value="GA">Georgia</option>
				<option value="HI">Hawaii</option>
				<option value="ID">Idaho</option>
				<option value="IL">Illinois</option>
				<option value="IN">Indiana</option>
				<option value="IA">Iowa</option>
				<option value="KS">Kansas</option>
				<option value="KY">Kentucky</option>
				<option value="LA">Louisiana</option>
				<option value="ME">Maine</option>
				<option value="MD">Maryland</option>
				<option value="MA">Massachusetts</option>
				<option value="MI">Michigan</option>
				<option value="MN">Minnesota</option>
				<option value="MS">Mississippi</option>
				<option value="MO">Missouri</option>
				<option value="MT">Montana</option>
				<option value="NE">Nebraska</option>
				<option value="NV">Nevada</option>
				<option value="NH">New Hampshire</option>
				<option value="NJ">New Jersey</option>
				<option value="NM">New Mexico</option>
				<option value="NY">New York</option>
				<option value="NC">North Carolina</option>
				<option value="ND">North Dakota</option>
				<option value="OH">Ohio</option>
				<option value="OK">Oklahoma</option>
				<option value="OR">Oregon</option>
				<option value="PA">Pennsylvania</option>
				<option value="RI">Rhode Island</option>
				<option value="SC">South Carolina</option>
				<option value="SD">South Dakota</option>
				<option value="TN">Tennessee</option>
				<option value="TX">Texas</option>
				<option value="UT">Utah</option>
				<option value="VT">Vermont</option>
				<option value="VA">Virginia</option>
				<option value="WA">Washington</option>
				<option value="WV">West Virginia</option>
				<option value="WI">Wisconsin</option>
				<option value="WY">Wyoming</option>
				</optgroup>
				<optgroup label="US Outlying Territories">
				<option value="AS">American Samoa</option>
				<option value="GU">Guam</option>
				<option value="MP">Northern Mariana Islands</option>
				<option value="PR">Puerto Rico</option>
				<option value="UM">US Minor Outlying Islands</option>
				<option value="VI">Virgin Islands</option>
				</optgroup>
				<optgroup label="Armed Forces">
				<option value="AA">Armed Forces Americas</option>
				<option value="AP">Armed Forces Pacific</option>
				<option value="AE">Armed Forces Others</option>
				</optgroup>& Territories"
				<optgroup label="Canadian Provinces & Territories">
				<option value="AB">Alberta</option>
				<option value="BC">British Columbia</option>
				<option value="MB">Manitoba</option>
				<option value="NB">New Brunswick</option>
				<option value="NL">Newfoundland and Labrador</option>
				<option value="NS">Nova Scotia</option>
				<option value="ON">Ontario</option>
				<option value="PE">Prince Edward Island</option>
				<option value="QC">Quebec</option>
				<option value="SK">Saskatchewan</option>
				<option value="NT">Northwest Territories</option>
				<option value="NU">Nunavut</option>
				<option value="YT">Yukon</option>
				</optgroup>
				<optgroup label="Mexican States">
				<option value="AGS">Aguascalientes</option>
				<option value="BCN">Baja California</option>
				<option value="BCS">Baja California Sur</option>
				<option value="CAM">Campeche</option>
				<option value="CHP">Chiapas</option>
				<option value="CHI">Chihuahua</option>
				<option value="COA">Coahuila</option>
				<option value="COL">Colima</option>
				<option value="DIF">Distrito Federal</option>
				<option value="DUR">Durango</option>
				<option value="MEX">Estado de M&eacute;xico</option>
				<option value="GTO">Guanajuato</option>
				<option value="GRO">Guerrero</option>
				<option value="HGO">Hidalgo</option>
				<option value="JAL">Jalisco</option>
				<option value="MIC">Michoac&aacute;n</option>
				<option value="MOR">Morelos</option>
				<option value="NAY">Nayarit</option>
				<option value="NLE">Nuevo Le&oacute;n</option>
				<option value="OAX">Oaxaca</option>
				<option value="PUE">Puebla</option>
				<option value="QUE">Quer&eacute;taro</option>
				<option value="ROO">Quintana Roo</option>
				<option value="SLP">San Luis Potos&iacute;</option>
				<option value="SIN">Sinaloa</option>
				<option value="SON">Sonora</option>
				<option value="TAB">Tabasco</option>
				<option value="TAM">Tamaulipas</option>
				<option value="TLA">Tlaxcala</option>
				<option value="VER">Veracruz</option>
				<option value="YUC">Yucat&aacute;n</option>
				<option value="ZAC">Zacatecas</option>
				</optgroup>
			</select>
	</div>
	<div>
		<label class="field" for="zip">Zip Code: </label>
		<input type="text" name="zip" size="5" maxlength="5" value="" />
	</div>
	</fieldset>
	<fieldset><legend>Position</legend>
	<div>
		<label class="field" for="position">Position: </label>
		<input type="text" name="position" size="30" value="" />
	</div>
	</fieldset>
	<div class="button">
		<button type="submit">Send</button>
	</div>
</form>