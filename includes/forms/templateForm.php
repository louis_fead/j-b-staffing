<form action="" method="post">
	<ul>
		<fieldset><legend></legend>
		<li>
		<div class="half">
				<label for="firstName">First Name:</label>
				<input type="text" id="firstName" name="firstName" class="input" />
			</div>
			<div class="half">
				<label for="lastName">Last Name:</label>
				<input type="text" id="lastName" name="lastName" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
				<label for="ssn"> Social Security Number (SSN): </label>
				<input type="text" id="ssn" name="ssn" class="input" />
			</div>
			<div class="half">
				<label for="phone"> Main Phone: </label>
				<input type="text" id="phone" name="phone" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
				<label for="email">Email:</label>
				<input type="text" id="email" name="email" class="input" />
			</div>
			<div class="half">
				<label for="phoneAlt"> Secondary Phone:</label>
				<input type="text" id="phoneAlt" name="phoneAlt" class="input" />
			</div>
		</li>
		<li>
			<div class="third">
				<label for="city">City:</label>
				<input type="text" id="city" name="city" class="input" />
			</div>
			<div class="third">
				<label for="state">State:</label>
				<select name="state" size="1" class="input" >
			</div>
			<div class="third omega">
				<label for="zip">Zip:</label>
				<input type="text" id="zip" name="zip" class="input" />
			</div>
		</li>
		<li>
			<div class="submit">
				<input type="submit" value="Submit">
			</div>
		</li>
	</ul>
	
	
</form>