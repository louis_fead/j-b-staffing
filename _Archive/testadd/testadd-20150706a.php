
<?php include("includes/overall/header.php"); ?>
	<title></title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
	<link href="css/formStylesheet.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	form {margin: 0 auto; width: 600px;}
	fieldset {width:608px;}
	form ul {margin:0; padding:0; list-style:none;}
	form li {margin-bottom:10px; overflow:hidden;}
	form label {display:block; margin-bottom:2px;}
	form label span {color:#999;}
	form .input {width:592px; border:1px solid #E0E0E0; background:#F6F6F6;}
	form select.input {border:1px solid #E0E0E0; background:#F6F6F6;}
	form .third {float:left; width:193px; margin-right:10px;}
	form .third .input {width:185px;}
	form .half {float:left; width:294px; margin-right:10px;}
	form .half .input {width:286px;}
	form .half select.input {width:294px;}
	form .omega {margin-right:0;}
	form .submit {text-align: center; width: 50%; margin: 0 auto;}
	</style>
  </head>
  <body>
	<?php include("includes/header.php"); ?>
	<?php //include("includes/menu.php"); ?>
	<?php if(isset($_POST['submit'])){
		
		$data_missing = array();
		
		if(empty($_POST['firstName'])){

			// Adds name to array
			$data_missing[] = 'First Name';

		} else {

			// Trim white space from the name and store the name
			$f_name = trim($_POST['firstName']);

		}

		if(empty($_POST['lastName'])){

			// Adds name to array
			$data_missing[] = 'Last Name';

		} else{

			// Trim white space from the name and store the name
			$l_name = trim($_POST['lastName']);

		}
		
		if(empty($_POST['phone'])){

			// Adds name to array
			$data_missing[] = 'Phone Number';

		} else {

			// Trim white space from the name and store the name
			$phone = trim($_POST['phone']);

		}
				
		if(empty($_POST['email'])){

			// Adds name to array
			$data_missing[] = 'Email';

		} else {

			// Trim white space from the name and store the name
			$email = trim($_POST['email']);

		}

		if(empty($_POST['street'])){

			// Adds name to array
			$data_missing[] = 'Street';

		} else {

			// Trim white space from the name and store the name
			$street = trim($_POST['street']);

		}

		if(empty($_POST['city'])){

			// Adds name to array
			$data_missing[] = 'City';

		} else {

			// Trim white space from the name and store the name
			$city = trim($_POST['city']);

		}

		if(empty($_POST['state'])){

			// Adds name to array
			$data_missing[] = 'State';

		} else {

			// Trim white space from the name and store the name
			$state = trim($_POST['state']);

		}

		if(empty($_POST['zip'])){

			// Adds name to array
			$data_missing[] = 'Zip Code';

		} else {

			// Trim white space from the name and store the name
			$zip = trim($_POST['zip']);

		}
		
		if(empty($_POST['Position'])){

			// Adds name to array
			$data_missing[] = 'Position';

		} else {

			// Trim white space from the name and store the name
			$position = trim($_POST['Position']);

		}
		
		if(empty($data_missing)){
			
			require_once('mysqli_connect.php');
			
			$query = "INSERT INTO test3 (firstName, lastName, phone, email, street, city, state, zip, position, dateCreated, ApplicantID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NULL)";
			
			$stmt = mysqli_prepare($dbc, $query);

	/*        
			i Integers
			d Doubles
			b Blobs
			s Everything Else
	*/        
			mysqli_stmt_bind_param($stmt, "sssssssis", $f_name, $l_name, $phone, $email, $street, $city, $state, $zip, $position);
			
			mysqli_stmt_execute($stmt);
			
			$affected_rows = mysqli_stmt_affected_rows($stmt);
			
			if($affected_rows == 1){
				
				echo 'Record Entered';
				
				mysqli_stmt_close($stmt);
				
				mysqli_close($dbc);
				
			} else {
				
				echo 'Error Occurred<br />';
				echo mysqli_error();
				
				mysqli_stmt_close($stmt);
				
				mysqli_close($dbc);
				
			}
			
		} else {
			
			echo 'You need to enter the following data<br />';
			
			foreach($data_missing as $missing){
				
				echo "$missing<br />";
				
			}
			
		}
		
	}

	?>
	<form action="http://localhost/jandbstafiing_com/testadd.php" method="post">
		<b>Add New Record</b>
		<ul>
		<fieldset><legend>Personal Info</legend>
		<li>
			<div class="half">
				<label for="firstName">First Name:</label>
				<input type="text" id="firstName" name="firstName" class="input" />
			</div>
			<div class="half">
				<label for="lastName">Last Name:</label>
				<input type="text" id="lastName" name="lastName" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
				<label for="ssn"> Social Security Number (SSN): </label>
				<input type="text" id="ssn" name="ssn" class="input" />
			</div>
			<div class="half">
				<label for="phone"> Main Phone: </label>
				<input type="text" id="phone" name="phone" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
				<label for="email">Email:</label>
				<input type="text" id="email" name="email" class="input" />
			</div>
			<div class="half">
				<label for="phoneAlt"> Secondary Phone:</label>
				<input type="text" id="phoneAlt" name="phoneAlt" class="input" />
			</div>
		</li>
		<li>
			<label for="street">Street:</label>
			<input type="text" id="street" name="street" class="input" />
		</li>
		<li>
			<div class="third">
				<label for="city">City:</label>
				<input type="text" id="city" name="city" class="input" />
			</div>
			<div class="third">
				<label for="state">State:</label>
				<select name="state" size="1" class="input" >
					<option selected disabled>SELECT STATE</option>
					<optgroup label="American States & Territories">
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District Of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
					</optgroup>
					<optgroup label="US Outlying Territories">
					<option value="AS">American Samoa</option>
					<option value="GU">Guam</option>
					<option value="MP">Northern Mariana Islands</option>
					<option value="PR">Puerto Rico</option>
					<option value="UM">US Minor Outlying Islands</option>
					<option value="VI">Virgin Islands</option>
					</optgroup>
					<optgroup label="Armed Forces">
					<option value="AA">Armed Forces Americas</option>
					<option value="AP">Armed Forces Pacific</option>
					<option value="AE">Armed Forces Others</option>
					</optgroup>& Territories"
					<optgroup label="Canadian Provinces & Territories">
					<option value="AB">Alberta</option>
					<option value="BC">British Columbia</option>
					<option value="MB">Manitoba</option>
					<option value="NB">New Brunswick</option>
					<option value="NL">Newfoundland and Labrador</option>
					<option value="NS">Nova Scotia</option>
					<option value="ON">Ontario</option>
					<option value="PE">Prince Edward Island</option>
					<option value="QC">Quebec</option>
					<option value="SK">Saskatchewan</option>
					<option value="NT">Northwest Territories</option>
					<option value="NU">Nunavut</option>
					<option value="YT">Yukon</option>
					</optgroup>
					<optgroup label="Mexican States">
					<option value="AGS">Aguascalientes</option>
					<option value="BCN">Baja California</option>
					<option value="BCS">Baja California Sur</option>
					<option value="CAM">Campeche</option>
					<option value="CHP">Chiapas</option>
					<option value="CHI">Chihuahua</option>
					<option value="COA">Coahuila</option>
					<option value="COL">Colima</option>
					<option value="DIF">Distrito Federal</option>
					<option value="DUR">Durango</option>
					<option value="MEX">Estado de M&eacute;xico</option>
					<option value="GTO">Guanajuato</option>
					<option value="GRO">Guerrero</option>
					<option value="HGO">Hidalgo</option>
					<option value="JAL">Jalisco</option>
					<option value="MIC">Michoac&aacute;n</option>
					<option value="MOR">Morelos</option>
					<option value="NAY">Nayarit</option>
					<option value="NLE">Nuevo Le&oacute;n</option>
					<option value="OAX">Oaxaca</option>
					<option value="PUE">Puebla</option>
					<option value="QUE">Quer&eacute;taro</option>
					<option value="ROO">Quintana Roo</option>
					<option value="SLP">San Luis Potos&iacute;</option>
					<option value="SIN">Sinaloa</option>
					<option value="SON">Sonora</option>
					<option value="TAB">Tabasco</option>
					<option value="TAM">Tamaulipas</option>
					<option value="TLA">Tlaxcala</option>
					<option value="VER">Veracruz</option>
					<option value="YUC">Yucat&aacute;n</option>
					<option value="ZAC">Zacatecas</option>
					</optgroup>
				</select>
			</div>
			<div class="third omega">
				<label for="zip">Zip:</label>
				<input type="text" id="zip" name="zip" class="input" />
			</div>
		</li>
		</fieldset>
		<fieldset><legend>Employment Overview</legend>
		<li>
			<div>
				<label for="PrevEmployedHere">Have You Ever Been Employed With J & B Staffing?</label>
				<input name="PrevEmployedHere" type="radio" value="Yes" /> Yes 
				<input name="PrevEmployedHere" type="radio" value="No" checked="checked" /> No
			</div>
		</li>
		<li>
			<div>
				<label for="JBCompaniesTemp">What Companies Did You Work For? </label>
				<textarea name="JBCompaniesTemp" rows="1" cols="19" style="width: 220px" /></textarea>
			</div>
		</li>
		<li>
			<div>
				<label for="Position">Position you are applying for *</label>
				<input name="Position" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="JB_Referred">How Were You Referred To J & B Staffing?</label>
				<input name="JB_Referred" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Days"><b>Which Days Are You Available To Work?</b></label>
				<input name="Availability_Days" type="checkbox" value="Monday" /> Monday <br />  
				<input name="Availability_Days" type="checkbox" value="Tuesday" /> Tuesday <br />  
				<input name="Availability_Days" type="checkbox" value="Wednesday" /> Wednesday <br />  
				<input name="Availability_Days" type="checkbox" value="Thursday" /> Thursday <br />  
				<input name="Availability_Days" type="checkbox" value="Friday" /> Friday <br />  
				<input name="Availability_Days" type="checkbox" value="Saturday" /> Saturday <br />  
				<input name="Availability_Days" type="checkbox" value="Sunday" /> Sunday <br />  
			</div>
		</li>
		<li>
			<div>
			<label for="Availability_Cities"><b>Cities You Are Available To Work In:</b></label>
			<input name="Availability_Cities" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Shifts"><b>Shifts Available:</b></label></td>
				<input name="Availability_Shifts" type="checkbox" value="1st" /> 1st  
				<input name="Availability_Shifts" type="checkbox" value="2nd" /> 2nd   
				<input name="Availability_Shifts" type="checkbox" value="3rd" /> 3rd 
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Start"><b>When are you available to start?</b></label>
				<input name="Availability_Start" type="date" />
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Other"><b>Please check any of the following that apply to you:</b></label>
				<input name="Availability_Other" type="checkbox" value="Longterm" /> Available for long term assignments <br />  
				<input name="Availability_Other" type="checkbox" value="Same_Day" /> Will accept same day assignments <br />  
				<input name="Availability_Other" type="checkbox" value="Temp2Perm" /> Available for temporary to permanent assignments <br /> 
				<input name="Availability_Other" type="checkbox" value="Car_Available" /> Car available <label for="OwnCar"> If yes, is this your own car? </label> 
				<input name="OwnCar" type="radio" value="Yes"/> Yes
				<input name="OwnCar" type="radio" value="No" /> No <br />
				<input name="Availability_Other" type="checkbox" value="Resume" /> Resume attached <br />
				<label for="Resume" ><input name="Resume" type="file" /><br />
			</div>
		</li>
		</fieldset>
		</ul>
		<p>
		<input type="submit" name="submit" value="Send" />
		</p>
	</form>
<?php include("includes/overall/footer.php"); ?>