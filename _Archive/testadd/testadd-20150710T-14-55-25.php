<?php include("includes/overall/header.php"); ?>
	<title></title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
	<link href="css/formStylesheet.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	form {margin: 0 auto; width: 600px;}
	fieldset {width:608px;}
	form ul {margin:0; padding:0; list-style:none;}
	form li {margin-bottom:10px; overflow:hidden;}
	form label {display:block; margin-bottom:2px;}
	form label span {color:#999;}
	form .input {width:592px; border:1px solid #E0E0E0; background:#F6F6F6;}
	form select.input {border:1px solid #E0E0E0; background:#F6F6F6;}
	form .third {float:left; width:193px; margin-right:10px;}
	form .third .input {width:185px;}
	form .half {float:left; width:294px; margin-right:10px;}
	form .half .input {width:286px;}
	form .half select.input {width:294px;}
	form .omega {margin-right:0;}
	form .submit {text-align: center; width: 50%; margin: 0 auto;}
	</style>
  </head>
  <body>
	<?php include("includes/header.php"); ?>
	<?php //include("includes/menu.php"); ?>
	<?php if(isset($_POST['submit'])){
		
		$default_message = "Nothing entered";
		$data_missing = array();
		
		//Personal Information 
		if(empty($_POST['firstName'])){

			// Adds name to array
			$data_missing[] = 'First Name';

		} else {

			// Trim white space from the name and store the name
			$f_name = trim($_POST['firstName']);

		}

		if(empty($_POST['lastName'])){

			// Adds name to array
			$data_missing[] = 'Last Name';

		} else{

			// Trim white space from the name and store the name
			$l_name = trim($_POST['lastName']);

		}
		
		if(empty($_POST['ssn'])){

			// Adds name to array
			$data_missing[] = 'SSN ';

		} else {

			// Trim white space from the name and store the name
			$ssn = trim($_POST['ssn']);

		}
		
		if(empty($_POST['phone'])){

			// Adds name to array
			$data_missing[] = 'Phone Number';

		} else {

			// Trim white space from the name and store the name
			$phone = trim($_POST['phone']);

		}
		
		if(empty($_POST['phoneAlt'])){

			// Adds name to array
			$data_missing[] = 'Alternate Phone ';

		} else {

			// Trim white space from the name and store the name
			$phoneAlt = trim($_POST['phoneAlt']);

		}
		
		if(empty($_POST['email'])){

			// Adds name to array
			$data_missing[] = 'Email';

		} else {

			// Trim white space from the name and store the name
			$email = trim($_POST['email']);

		}

		if(empty($_POST['street'])){

			// Adds name to array
			$data_missing[] = 'Street';

		} else {

			// Trim white space from the name and store the name
			$street = trim($_POST['street']);

		}

		if(empty($_POST['city'])){

			// Adds name to array
			$data_missing[] = 'City';

		} else {

			// Trim white space from the name and store the name
			$city = trim($_POST['city']);

		}

		if(empty($_POST['state'])){

			// Adds name to array
			$data_missing[] = 'State';

		} else {

			// Trim white space from the name and store the name
			$state = trim($_POST['state']);

		}

		if(empty($_POST['zip'])){

			// Adds name to array
			$data_missing[] = 'Zip Code';

		} else {

			// Trim white space from the name and store the name
			$zip = trim($_POST['zip']);

		}
		
		//In Case of Emergency 
		if(empty($_POST['ICE_Name'])){

			// Adds name to array
			$data_missing[] = 'ICE_Name';

		} else {

			// Trim white space from the name and store the name
			$ICE_Name = trim($_POST['ICE_Name']);

		}
		
		if(empty($_POST['ICE_Phone'])){

			// Adds name to array
			$data_missing[] = 'ICE_Phone';

		} else {

			// Trim white space from the name and store the name
			$ICE_Phone = trim($_POST['ICE_Phone']);

		}
		
		if(empty($_POST['ICE_Address'])){

			// Adds name to array
			$data_missing[] = 'ICE_Address';

		} else {

			// Trim white space from the name and store the name
			$ICE_Address = trim($_POST['ICE_Address']);

		}
		
		//Employment Overview
		if(empty($_POST['PrevEmployedHere'])){

			// Adds name to array
			$data_missing[] = 'PrevEmployedHere';

		} else {

			// Trim white space from the name and store the name
			$PrevEmployedHere = trim($_POST['PrevEmployedHere']);
		}
		
		if(empty($_POST['JBCompaniesTemp'])){

			// Adds name to array
//			$data_missing[] = 'JBCompaniesTemp';
			//Assigns default value to name
			$_POST['JBCompaniesTemp'] = $default_message;
			// Trim white space from the name and store the name
			$JBCompaniesTemp = trim($_POST['JBCompaniesTemp']);

		} else {

			// Trim white space from the name and store the name
			$JBCompaniesTemp = trim($_POST['JBCompaniesTemp']);

		}
		
		if(empty($_POST['Position'])){

			// Adds name to array
			$data_missing[] = 'Position';

		} else {

			// Trim white space from the name and store the name
			$position = trim($_POST['Position']);

		}
		
		if(empty($_POST['JB_Referred'])){

			// Adds name to array
			$data_missing[] = 'JB_Referred';

		} else {

			// Trim white space from the name and store the name
			$JB_Referred = trim($_POST['JB_Referred']);

		}
		
		//Availability_Days
		
		if(empty($_POST['Availability_Days'])){

			// Adds name to array
			$data_missing[] = 'Availability_Days';

		} else {

			// Trim white space from the name and store the name
			$Availability_Days = trim($_POST['Availability_Days']);

		}
		
		if(empty($_POST['Availability_Cities'])){

			// Adds name to array
			$data_missing[] = 'Availability_Cities';

		} else {

			// Trim white space from the name and store the name
			$Availability_Cities = trim($_POST['Availability_Cities']);

		}
		
		//Availability_Shifts

		if(empty($_POST['Availability_Shifts'])){

			// Adds name to array
			$data_missing[] = 'Availability_Shifts';

		} else {

			// Trim white space from the name and store the name
			$Availability_Shifts = trim($_POST['Availability_Shifts']);

		}
		
		if(empty($_POST['Availability_Start'])){

			// Adds name to array
			$data_missing[] = 'Availability_Start';

		} else {

			// Trim white space from the name and store the name
			$Availability_Start = trim($_POST['Availability_Start']);

		}
		
		//Availability_Other
/*
		if(empty($_POST['Availability_Other'])){

			// Adds name to array
			$data_missing[] = 'Availability_Other';

		} else {

			// Trim white space from the name and store the name
			$Availability_Other = trim($_POST['Availability_Other']);

		}
*/		
		if(empty($_POST['Longterm'])){

			// Adds name to array
			$data_missing[] = 'Longterm';

		} else {

			// Trim white space from the name and store the name
			$Longterm = trim($_POST['Longterm']);

		}
		
		if(empty($_POST['Same_Day'])){

			// Adds name to array
			$data_missing[] = 'Same_Day';

		} else {

			// Trim white space from the name and store the name
			$Same_Day = trim($_POST['Same_Day']);

		}
		
		if(empty($_POST['Temp2Perm'])){

			// Adds name to array
			$data_missing[] = 'Temp2Perm';

		} else {

			// Trim white space from the name and store the name
			$Temp2Perm = trim($_POST['Temp2Perm']);

		}
		
		if(empty($_POST['Car_Available'])){

			// Adds name to array
			$data_missing[] = 'Car_Available';

		} else {

			// Trim white space from the name and store the name
			$Car_Available = trim($_POST['Car_Available']);

		}
		
		if(empty($_POST['OwnCar'])){

			// Adds name to array
			$data_missing[] = 'OwnCar';

		} else {

			// Trim white space from the name and store the name
			$OwnCar = trim($_POST['OwnCar']);

		}
		
		if(empty($_POST['Resume_Attached'])){

			// Adds name to array
//			$data_missing[] = 'Resume_Attached';
			$_POST['Resume_Attached'] = $default_message;
			// Trim white space from the name and store the name
			$Resume_Attached = trim($_POST['Resume_Attached']);

		} else {

			// Trim white space from the name and store the name
			$Resume_Attached = trim($_POST['Resume_Attached']);

		}
	
		//Work Eligiblity
		if(empty($_POST['Work_Eligiblity'])){

			// Adds name to array
			$data_missing[] = 'Work_Eligiblity';

		} else {

			// Trim white space from the name and store the name
			$Work_Eligiblity = trim($_POST['Work_Eligiblity']);

		}
		
		if(empty($_POST['Majority'])){

			// Adds name to array
			$data_missing[] = 'Majority';

		} else {

			// Trim white space from the name and store the name
			$Majority = trim($_POST['Majority']);

		}
		
		if(empty($_POST['Felony'])){

			// Adds name to array
			$data_missing[] = 'Felony';

		} else {

			// Trim white space from the name and store the name
			$Felony = trim($_POST['Felony']);

		}
		
		if(empty($_POST['Felony_Explaination'])){

			// Adds name to array
//			$data_missing[] = 'Felony_Explaination';
			//Assigns default value to name
			$_POST['Felony_Explaination'] = $default_message;
			// Trim white space from the name and store the name
			$Felony_Explaination = trim($_POST['Felony_Explaination']);

		} else {

			// Trim white space from the name and store the name
			$Felony_Explaination = trim($_POST['Felony_Explaination']);

		}
		
		//Education Background
		if(empty($_POST['High_School'])){

			// Adds name to array
			$data_missing[] = 'High_School';

		} else {

			// Trim white space from the name and store the name
			$High_School = trim($_POST['High_School']);

		}
		
		if(empty($_POST['High_School_GraduationDate'])){

			// Adds name to array
			$data_missing[] = 'High_School_GraduationDate';

		} else {

			// Trim white space from the name and store the name
			$High_School_GraduationDate = trim($_POST['High_School_GraduationDate']);

		}
		
		if(empty($_POST['College'])){

			// Adds name to array
			$data_missing[] = 'College';

		} else {

			// Trim white space from the name and store the name
			$College = trim($_POST['College']);

		}
		
		if(empty($_POST['College_GraduationDate'])){

			// Adds name to array
			$data_missing[] = 'College_GraduationDate';

		} else {

			// Trim white space from the name and store the name
			$College_GraduationDate = trim($_POST['College_GraduationDate']);

		}
		
		if(empty($_POST['Degree_Emphasis'])){

			// Adds name to array
			$data_missing[] = 'Degree_Emphasis';

		} else {

			// Trim white space from the name and store the name
			$Degree_Emphasis = trim($_POST['Degree_Emphasis']);

		}
		
		if(empty($_POST['Special_Skills'])){

			// Adds name to array
			$data_missing[] = 'Special_Skills';

		} else {

			// Trim white space from the name and store the name
			$Special_Skills = trim($_POST['Special_Skills']);

		}
		
		//Previous Employment History
		//--	1st Employer	--
		if(empty($_POST['1stEmployer_Name'])){

			// Adds name to array
			$data_missing[] = '1stEmployer_Name';

		} else {

			// Trim white space from the name and store the name
			$Employer1_Name = trim($_POST['1stEmployer_Name']);

		}
		
		if(empty($_POST['1stEmployer_Address'])){

			// Adds name to array
			$data_missing[] = '1stEmployer_Address';

		} else {

			// Trim white space from the name and store the name
			$Employer1_Address = trim($_POST['1stEmployer_Address']);

		}
		
		if(empty($_POST['1stEmployer_Supervisor'])){

			// Adds name to array
			$data_missing[] = '1stEmployer_Supervisor';

		} else {

			// Trim white space from the name and store the name
			$Employer1_Supervisor = trim($_POST['1stEmployer_Supervisor']);

		}
		
		if(empty($_POST['1stEmployer_Phone'])){

			// Adds name to array
			$data_missing[] = '1stEmployer_Phone';

		} else {

			// Trim white space from the name and store the name
			$Employer1_Phone = trim($_POST['1stEmployer_Phone']);

		}
		
		if(empty($_POST['1stEmployer_PayRate'])){

			// Adds name to array
			$data_missing[] = '1stEmployer_PayRate';

		} else {

			// Trim white space from the name and store the name
			$Employer1_PayRate = trim($_POST['1stEmployer_PayRate']);

		}
		
		if(empty($_POST['1stEmployer_DatesEmployed'])){

			// Adds name to array
			$data_missing[] = '1stEmployer_DatesEmployed';

		} else {

			// Trim white space from the name and store the name
			$Employer1_DatesEmployed = trim($_POST['1stEmployer_DatesEmployed']);

		}
		
		if(empty($_POST['1stEmployer_Job'])){

			// Adds name to array
			$data_missing[] = '1stEmployer_Job';

		} else {

			// Trim white space from the name and store the name
			$Employer1_Job = trim($_POST['1stEmployer_Job']);

		}
		
		if(empty($_POST['1stEmployer_Reason'])){

			// Adds name to array
			$data_missing[] = '1stEmployer_Reason';

		} else {

			// Trim white space from the name and store the name
			$Employer1_Reason = trim($_POST['1stEmployer_Reason']);

		}
		
		//--	2nd Employer	--		
		if(empty($_POST['2ndEmployer_Name'])){

			// Adds name to array
//			$data_missing[] = '2ndEmployer_Name';
			$_POST['2ndEmployer_Name'] = $default_message;
			// Trim white space from the name and store the name
			$Employer2_Name = trim($_POST['2ndEmployer_Name']);

		} else {

			// Trim white space from the name and store the name
			$Employer2_Name = trim($_POST['2ndEmployer_Name']);

		}
		
		if(empty($_POST['2ndEmployer_Address'])){

			// Adds name to array
//			$data_missing[] = '2ndEmployer_Address';
			$_POST['2ndEmployer_Address'] = $default_message;
			// Trim white space from the name and store the name
			$Employer2_Address = trim($_POST['2ndEmployer_Address']);

		} else {

			// Trim white space from the name and store the name
			$Employer2_Address = trim($_POST['2ndEmployer_Address']);

		}
		
		if(empty($_POST['2ndEmployer_Supervisor'])){

			// Adds name to array
//			$data_missing[] = '2ndEmployer_Supervisor';
			$_POST['2ndEmployer_Supervisor'] = $default_message;
			// Trim white space from the name and store the name
			$Employer2_Supervisor = trim($_POST['2ndEmployer_Supervisor']);

		} else {

			// Trim white space from the name and store the name
			$Employer2_Supervisor = trim($_POST['2ndEmployer_Supervisor']);

		}
		
		if(empty($_POST['2ndEmployer_Phone'])){

			// Adds name to array
//			$data_missing[] = '2ndEmployer_Phone';
			$_POST['2ndEmployer_Phone'] = $default_message;
			// Trim white space from the name and store the name
			$Employer2_Phone = trim($_POST['2ndEmployer_Phone']);

		} else {

			// Trim white space from the name and store the name
			$Employer2_Phone = trim($_POST['2ndEmployer_Phone']);

		}
		
		if(empty($_POST['2ndEmployer_PayRate'])){

			// Adds name to array
//			$data_missing[] = '2ndEmployer_PayRate';
			$_POST['2ndEmployer_PayRate'] = $default_message;
			// Trim white space from the name and store the name
			$Employer2_PayRate = trim($_POST['2ndEmployer_PayRate']);

		} else {

			// Trim white space from the name and store the name
			$Employer2_PayRate = trim($_POST['2ndEmployer_PayRate']);

		}
		
		if(empty($_POST['2ndEmployer_DatesEmployed'])){

			// Adds name to array
//			$data_missing[] = '2ndEmployer_DatesEmployed';
			$_POST['2ndEmployer_DatesEmployed'] = $default_message;
			// Trim white space from the name and store the name
			$Employer2_DatesEmployed = trim($_POST['2ndEmployer_DatesEmployed']);

		} else {

			// Trim white space from the name and store the name
			$Employer2_DatesEmployed = trim($_POST['2ndEmployer_DatesEmployed']);

		}
		
		if(empty($_POST['2ndEmployer_Job'])){

			// Adds name to array
//			$data_missing[] = '2ndEmployer_Job';
			$_POST['2ndEmployer_Job'] = $default_message;
			// Trim white space from the name and store the name
			$Employer2_Job = trim($_POST['2ndEmployer_Job']);

		} else {

			// Trim white space from the name and store the name
			$Employer2_Job = trim($_POST['2ndEmployer_Job']);

		}
		
		if(empty($_POST['2ndEmployer_Reason'])){

			// Adds name to array
//			$data_missing[] = '2ndEmployer_Reason';
			$_POST['2ndEmployer_Reason'] = $default_message;
			// Trim white space from the name and store the name
			$Employer2_Reason = trim($_POST['2ndEmployer_Reason']);

		} else {

			// Trim white space from the name and store the name
			$Employer2_Reason = trim($_POST['2ndEmployer_Reason']);

		}
		
		//--	3rd Employer	--
		if(empty($_POST['3rdEmployer_Name'])){

			// Adds name to array
//			$data_missing[] = '3rdEmployer_Name';
			$_POST['3rdEmployer_Name'] = $default_message;
			// Trim white space from the name and store the name
			$Employer3_Name = trim($_POST['3rdEmployer_Name']);

		} else {

			// Trim white space from the name and store the name
			$Employer3_Name = trim($_POST['3rdEmployer_Name']);

		}
		
		if(empty($_POST['3rdEmployer_Address'])){

			// Adds name to array
//			$data_missing[] = '3rdEmployer_Address';
			$_POST['3rdEmployer_Address'] = $default_message;
			// Trim white space from the name and store the name
			$Employer3_Address = trim($_POST['3rdEmployer_Address']);

		} else {

			// Trim white space from the name and store the name
			$Employer3_Address = trim($_POST['3rdEmployer_Address']);

		}
		
		if(empty($_POST['3rdEmployer_Supervisor'])){

			// Adds name to array
//			$data_missing[] = '3rdEmployer_Supervisor';
			$_POST['3rdEmployer_Supervisor'] = $default_message;
			// Trim white space from the name and store the name
			$Employer3_Supervisor = trim($_POST['3rdEmployer_Supervisor']);

		} else {

			// Trim white space from the name and store the name
			$Employer3_Supervisor = trim($_POST['3rdEmployer_Supervisor']);

		}
		
		if(empty($_POST['3rdEmployer_Phone'])){

			// Adds name to array
//			$data_missing[] = '3rdEmployer_Phone';
			$_POST['3rdEmployer_Phone'] = $default_message;
			// Trim white space from the name and store the name
			$Employer3_Phone = trim($_POST['3rdEmployer_Phone']);

		} else {

			// Trim white space from the name and store the name
			$Employer3_Phone = trim($_POST['3rdEmployer_Phone']);

		}
		
		if(empty($_POST['3rdEmployer_PayRate'])){

			// Adds name to array
//			$data_missing[] = '3rdEmployer_PayRate';
			$_POST['3rdEmployer_PayRate'] = $default_message;
			// Trim white space from the name and store the name
			$Employer3_PayRate = trim($_POST['3rdEmployer_PayRate']);

		} else {

			// Trim white space from the name and store the name
			$Employer3_PayRate = trim($_POST['3rdEmployer_PayRate']);

		}
		
		if(empty($_POST['3rdEmployer_DatesEmployed'])){

			// Adds name to array
//			$data_missing[] = '3rdEmployer_DatesEmployed';
			$_POST['3rdEmployer_DatesEmployed'] = $default_message;
			// Trim white space from the name and store the name
			$Employer3_DatesEmployed = trim($_POST['3rdEmployer_DatesEmployed']);

		} else {

			// Trim white space from the name and store the name
			$Employer3_DatesEmployed = trim($_POST['3rdEmployer_DatesEmployed']);

		}
		
		if(empty($_POST['3rdEmployer_Job'])){

			// Adds name to array
//			$data_missing[] = '3rdEmployer_Job';
			$_POST['3rdEmployer_Job'] = $default_message;
			// Trim white space from the name and store the name
			$Employer3_Job = trim($_POST['3rdEmployer_Job']);

		} else {

			// Trim white space from the name and store the name
			$Employer3_Job = trim($_POST['3rdEmployer_Job']);

		}
		
		if(empty($_POST['3rdEmployer_Reason'])){

			// Adds name to array
//			$data_missing[] = '3rdEmployer_Reason';
			$_POST['3rdEmployer_Reason'] = $default_message;
			// Trim white space from the name and store the name
			$Employer3_Reason = trim($_POST['3rdEmployer_Reason']);

		} else {

			// Trim white space from the name and store the name
			$Employer3_Reason = trim($_POST['3rdEmployer_Reason']);

		}
		
		//--	4th Employer	--	
		if(empty($_POST['4thEmployer_Name'])){

			// Adds name to array
//			$data_missing[] = '4thEmployer_Name';
			$_POST['4thEmployer_Name'] = $default_message;
			// Trim white space from the name and store the name
			$Employer4_Name = trim($_POST['4thEmployer_Name']);

		} else {

			// Trim white space from the name and store the name
			$Employer4_Name = trim($_POST['4thEmployer_Name']);

		}
		
		if(empty($_POST['4thEmployer_Address'])){

			// Adds name to array
//			$data_missing[] = '4thEmployer_Address';
			$_POST['4thEmployer_Address'] = $default_message;
			// Trim white space from the name and store the name
			$Employer4_Address = trim($_POST['4thEmployer_Address']);

		} else {

			// Trim white space from the name and store the name
			$Employer4_Address = trim($_POST['4thEmployer_Address']);

		}
		
		if(empty($_POST['4thEmployer_Supervisor'])){

			// Adds name to array
//			$data_missing[] = '4thEmployer_Supervisor';
			$_POST['4thEmployer_Supervisor'] = $default_message;
			// Trim white space from the name and store the name
			$Employer4_Supervisor = trim($_POST['4thEmployer_Supervisor']);

		} else {

			// Trim white space from the name and store the name
			$Employer4_Supervisor = trim($_POST['4thEmployer_Supervisor']);

		}
		
		if(empty($_POST['4thEmployer_Phone'])){

			// Adds name to array
//			$data_missing[] = '4thEmployer_Phone';
			$_POST['4thEmployer_Phone'] = $default_message;
			// Trim white space from the name and store the name
			$Employer4_Phone = trim($_POST['4thEmployer_Phone']);

		} else {

			// Trim white space from the name and store the name
			$Employer4_Phone = trim($_POST['4thEmployer_Phone']);

		}
		
		if(empty($_POST['4thEmployer_PayRate'])){

			// Adds name to array
//			$data_missing[] = '4thEmployer_PayRate';
			//Assigns default value to name
			$_POST['4thEmployer_PayRate'] = $default_message;
			// Trim white space from the name and store the name
			$Employer4_PayRate = trim($_POST['4thEmployer_PayRate']);

		} else {

			// Trim white space from the name and store the name
			$Employer4_PayRate = trim($_POST['4thEmployer_PayRate']);

		}
		
		if(empty($_POST['4thEmployer_DatesEmployed'])){

			// Adds name to array
//			$data_missing[] = '4thEmployer_DatesEmployed';
			//Assigns default value to name
			$_POST['4thEmployer_DatesEmployed'] = $default_message;
			// Trim white space from the name and store the name
			$Employer4_DatesEmployed = trim($_POST['4thEmployer_DatesEmployed']);

		} else {

			// Trim white space from the name and store the name
			$Employer4_DatesEmployed = trim($_POST['4thEmployer_DatesEmployed']);

		}
		
		if(empty($_POST['4thEmployer_Job'])){

			// Adds name to array
//			$data_missing[] = '4thEmployer_Job';
			//Assigns default value to name
			$_POST['4thEmployer_Job'] = $default_message;
			// Trim white space from the name and store the name
			$Employer4_Job = trim($_POST['4thEmployer_Job']);

		} else {

			// Trim white space from the name and store the name
			$Employer4_Job = trim($_POST['4thEmployer_Job']);

		}
		
		if(empty($_POST['4thEmployer_Reason'])){

			// Adds name to array
//			$data_missing[] = '4thEmployer_Reason';
			//Assigns default value to name
			$_POST['4thEmployer_Reason'] = $default_message;
			// Trim white space from the name and store the name
			$Employer4_Reason = trim($_POST['4thEmployer_Reason']);

		} else {

			// Trim white space from the name and store the name
			$Employer4_Reason = trim($_POST['4thEmployer_Reason']);

		}
		
		//References
		if(empty($_POST['1stReference_Name'])){

			// Adds name to array
			$data_missing[] = '1stReference_Name';

		} else {

			// Trim white space from the name and store the name
			$Reference1_Name = trim($_POST['1stReference_Name']);

		}
		
		if(empty($_POST['1stReference_Address'])){

			// Adds name to array
			$data_missing[] = '1stReference_Address';

		} else {

			// Trim white space from the name and store the name
			$Reference1_Address = trim($_POST['1stReference_Address']);

		}
		
		if(empty($_POST['1stReference_Phone'])){

			// Adds name to array
			$data_missing[] = '1stReference_Phone';

		} else {

			// Trim white space from the name and store the name
			$Reference1_Phone = trim($_POST['1stReference_Phone']);

		}
		
		if(empty($_POST['1stReference_Relation'])){

			// Adds name to array
			$data_missing[] = '1stReference_Relation';

		} else {

			// Trim white space from the name and store the name
			$Reference1_Relation = trim($_POST['1stReference_Relation']);

		}
		
		if(empty($_POST['2ndReference_Name'])){

			// Adds name to array
			$data_missing[] = '2ndReference_Name';

		} else {

			// Trim white space from the name and store the name
			$Reference2_Name = trim($_POST['2ndReference_Name']);

		}
		
		if(empty($_POST['2ndReference_Address'])){

			// Adds name to array
			$data_missing[] = '2ndReference_Address';

		} else {

			// Trim white space from the name and store the name
			$Reference2_Address = trim($_POST['2ndReference_Address']);

		}
		
		if(empty($_POST['2ndReference_Phone'])){

			// Adds name to array
			$data_missing[] = '2ndReference_Phone';

		} else {

			// Trim white space from the name and store the name
			$Reference2_Phone = trim($_POST['2ndReference_Phone']);

		}
		
		if(empty($_POST['2ndReference_Relation'])){

			// Adds name to array
			$data_missing[] = '2ndReference_Relation';

		} else {

			// Trim white space from the name and store the name
			$Reference2_Relation = trim($_POST['2ndReference_Relation']);

		}
		
		//Temporary Services
		if(empty($_POST['1stService_Name'])){

			// Adds name to array
//			$data_missing[] = '1stService_Name';
			//Assigns default value to name
			$_POST['1stService_Name'] = $default_message;
			// Trim white space from the name and store the name
			$Service1_Name = trim($_POST['1stService_Name']);

		} else {

			// Trim white space from the name and store the name
			$Service1_Name = trim($_POST['1stService_Name']);

		}
		
		if(empty($_POST['1stService_Assigned_Companies'])){

			// Adds name to array
//			$data_missing[] = '1stService_Assigned_Companies';
			//Assigns default value to name
			$_POST['1stService_Assigned_Companies'] = $default_message;
			// Trim white space from the name and store the name
			$Service1_Assigned_Companies = trim($_POST['1stService_Assigned_Companies']);

		} else {

			// Trim white space from the name and store the name
			$Service1_Assigned_Companies = trim($_POST['1stService_Assigned_Companies']);
			

		}
		
		if(empty($_POST['2ndService_Name'])){

			// Adds name to array
//			$data_missing[] = '2ndService_Name';
			//Assigns default value to name
			$_POST['2ndService_Name'] = $default_message;
			// Trim white space from the name and store the name
			$Service2_Name = trim($_POST['2ndService_Name']);

		} else {

			// Trim white space from the name and store the name
			$Service2_Name = trim($_POST['2ndService_Name']);

		}
		
		if(empty($_POST['2ndService_Assigned_Companies'])){

			// Adds name to array
//			$data_missing[] = '2ndService_Assigned_Companies';
			//Assigns default value to name
			$_POST['2ndService_Assigned_Companies'] = $default_message;
			// Trim white space from the name and store the name
			$Service2_Assigned_Companies = trim($_POST['2ndService_Assigned_Companies']);

		} else {

			// Trim white space from the name and store the name
			$Service2_Assigned_Companies = trim($_POST['2ndService_Assigned_Companies']);

		}
		
		if(empty($_POST['3rdService_Name'])){

			// Adds name to array
//			$data_missing[] = '3rdService_Name';
			//Assigns default value to name
			$_POST['3rdService_Name'] = $default_message;
			// Trim white space from the name and store the name
			$Service3_Name = trim($_POST['3rdService_Name']);

		} else {

			// Trim white space from the name and store the name
			$Service3_Name = trim($_POST['3rdService_Name']);

		}
		
		if(empty($_POST['3rdService_Assigned_Companies'])){

			// Adds name to array
//			$data_missing[] = '3rdService_Assigned_Companies';
			//Assigns default value to name
			$_POST['3rdService_Assigned_Companies'] = $default_message;
			// Trim white space from the name and store the name
			$Service3_Assigned_Companies = trim($_POST['3rdService_Assigned_Companies']);

		} else {

			// Trim white space from the name and store the name
			$Service3_Assigned_Companies = trim($_POST['3rdService_Assigned_Companies']);

		}
		
		//Certification
		if(empty($_POST['Signature'])){

			// Adds name to array
			$data_missing[] = 'Signature';

		} else {

			// Trim white space from the name and store the name
			$Signature = trim($_POST['Signature']);

		}
		
		if(empty($_POST['Signature_Date'])){

			// Adds name to array
			$data_missing[] = 'Signature_Date';

		} else {

			// Trim white space from the name and store the name
			$Signature_Date = trim($_POST['Signature_Date']);

		}
		

		if(empty($data_missing)){
			
			require_once('mysqli_connect.php');
			
			$query = "INSERT INTO test3 (
			firstName, lastName, ssn, phone, phoneAlt, email, street, city, state, zip, 
			ICE_Name, ICE_Phone, ICE_Address,
			position, PrevEmployedHere, JBCompaniesTemp, JB_Referred, Availability_Days, Availability_Cities, Availability_Shifts, Availability_Start,
			Longterm, Same_Day, Temp2Perm, Car_Available, OwnCar, Resume_Attached,
			Work_Eligiblity, Majority, Felony, Felony_Explaination,
			High_School, High_School_GraduationDate, College, College_GraduationDate, Degree_Emphasis, Special_Skills,
			1stEmployer_Name, 1stEmployer_Address, 1stEmployer_Supervisor, 1stEmployer_Phone, 1stEmployer_PayRate, 1stEmployer_DatesEmployed, 1stEmployer_Job, 1stEmployer_Reason,
			2ndEmployer_Name, 2ndEmployer_Address, 2ndEmployer_Supervisor, 2ndEmployer_Phone, 2ndEmployer_PayRate, 2ndEmployer_DatesEmployed, 2ndEmployer_Job, 2ndEmployer_Reason,
			3rdEmployer_Name, 3rdEmployer_Address, 3rdEmployer_Supervisor, 3rdEmployer_Phone, 3rdEmployer_PayRate, 3rdEmployer_DatesEmployed, 3rdEmployer_Job, 3rdEmployer_Reason,
			4thEmployer_Name, 4thEmployer_Address, 4thEmployer_Supervisor, 4thEmployer_Phone, 4thEmployer_PayRate, 4thEmployer_DatesEmployed, 4thEmployer_Job, 4thEmployer_Reason,
			1stReference_Name, 1stReference_Address, 1stReference_Phone, 1stReference_Relation,
			2ndReference_Name, 2ndReference_Address, 2ndReference_Phone, 2ndReference_Relation,
			1stService_Name, 1stService_Assigned_Companies, 2ndService_Name, 2ndService_Assigned_Companies, 3rdService_Name, 3rdService_Assigned_Companies,
			Signature, Signature_Date,
			dateCreated, ApplicantID) 
			VALUES (
			?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
			?, ?, ?,
			?, ?, ?, ?, ?, ?, ?, ?,
			?, ?, ?, ?, ?, ?,
			?, ?, ?, ?,
			?, ?, ?, ?, ?, ?,
			?, ?, ?, ?, ?, ?, ?, ?, 
			?, ?, ?, ?, ?, ?, ?, ?, 
			?, ?, ?, ?, ?, ?, ?, ?,
			?, ?, ?, ?, ?, ?, ?, ?, 
			?, ?, ?, ?,
			?, ?, ?, ?,
			?, ?, ?, ?, ?, ?,
			?, ?,
			NOW(), NULL)";
			
			$stmt = mysqli_prepare($dbc, $query);

	/*        
			i Integers
			d Doubles
			b Blobs
			s Everything Else
	*/

			mysqli_stmt_bind_param($stmt, "sssssssssisssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss", 
			//Personal Information
			$f_name, $l_name, $ssn, $phone, $phoneAlt, $email, $street, $city, $state, $zip, 
			//In Case of Emergency 
			$ICE_Name, $ICE_Phone, $ICE_Address,
			//Employment Overview
			$position, $PrevEmployedHere, $JBCompaniesTemp, $JB_Referred, $Availability_Days, $Availability_Cities, $Availability_Shifts, $Availability_Start,
			//Availability_Other
			$Longterm, $Same_Day, $Temp2Perm, $Car_Available, $OwnCar, $Resume_Attached,
			//Work Eligiblity
			$Work_Eligiblity, $Majority, $Felony, $Felony_Explaination, 
			//Education Background
			$High_School, $High_School_GraduationDate, $College, $College_GraduationDate, $Degree_Emphasis, $Special_Skills,
			//Previous Employment History
			//--	1st Employer	--
			$Employer1_Name, $Employer1_Address, $Employer1_Supervisor, $Employer1_Phone, $Employer1_PayRate, $Employer1_DatesEmployed, $Employer1_Job, $Employer1_Reason,
			//--	2nd Employer	--
			$Employer2_Name, $Employer2_Address, $Employer2_Supervisor, $Employer2_Phone, $Employer2_PayRate, $Employer2_DatesEmployed, $Employer2_Job, $Employer2_Reason,
			//--	3rd Employer	--
			$Employer3_Name, $Employer3_Address, $Employer3_Supervisor, $Employer3_Phone, $Employer3_PayRate, $Employer3_DatesEmployed, $Employer3_Job, $Employer3_Reason,
			//--	4th Employer	--
			$Employer4_Name, $Employer4_Address, $Employer4_Supervisor, $Employer4_Phone, $Employer4_PayRate, $Employer4_DatesEmployed, $Employer4_Job, $Employer4_Reason,
			//References
			$Reference1_Name, $Reference1_Address, $Reference1_Phone, $Reference1_Relation,
			$Reference2_Name, $Reference2_Address, $Reference2_Phone, $Reference2_Relation,
			//Temporary Services
			$Service1_Name, $Service1_Assigned_Companies,
			$Service2_Name, $Service2_Assigned_Companies,
			$Service3_Name, $Service3_Assigned_Companies,
			//Certification
			$Signature, $Signature_Date
			);
			
			mysqli_stmt_execute($stmt);
			
			$affected_rows = mysqli_stmt_affected_rows($stmt);
			
			if($affected_rows == 1){
				
				echo 'Record Entered';
				
				mysqli_stmt_close($stmt);
				
				mysqli_close($dbc);
				
			} else {
				
				echo 'Error Occurred<br />';
				echo mysqli_error();
				
				mysqli_stmt_close($stmt);
				
				mysqli_close($dbc);
				
			}
			
		} else {
			
			echo 'You need to enter the following data<br />';
			
			foreach($data_missing as $missing){
				
				echo "$missing<br />";
				
			}
			
		}
		
	}

	?>
	<form action="http://localhost/jandbstafiing_com/testadd.php" method="post">
		<b>Add New Record</b>
		<ul>
		<fieldset><legend>Personal Info</legend>
		<li>
			<div class="half">
				<label for="firstName">First Name:</label>
				<input type="text" id="firstName" name="firstName" class="input" />
			</div>
			<div class="half">
				<label for="lastName">Last Name:</label>
				<input type="text" id="lastName" name="lastName" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
				<label for="ssn"> Social Security Number (SSN): </label>
				<input type="text" id="ssn" name="ssn" class="input" />
			</div>
			<div class="half">
				<label for="phone"> Main Phone: </label>
				<input type="text" id="phone" name="phone" class="input" />
			</div>
		</li>
		<li>
			<div class="half">
				<label for="email">Email:</label>
				<input type="text" id="email" name="email" class="input" />
			</div>
			<div class="half">
				<label for="phoneAlt"> Secondary Phone:</label>
				<input type="text" id="phoneAlt" name="phoneAlt" class="input" />
			</div>
		</li>
		<li>
			<label for="street">Street:</label>
			<input type="text" id="street" name="street" class="input" />
		</li>
		<li>
			<div class="third">
				<label for="city">City:</label>
				<input type="text" id="city" name="city" class="input" />
			</div>
			<div class="third">
				<label for="state">State:</label>
				<select name="state" size="1" class="input" >
					<option selected disabled>SELECT STATE</option>
					<optgroup label="American States & Territories">
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District Of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
					</optgroup>
					<optgroup label="US Outlying Territories">
					<option value="AS">American Samoa</option>
					<option value="GU">Guam</option>
					<option value="MP">Northern Mariana Islands</option>
					<option value="PR">Puerto Rico</option>
					<option value="UM">US Minor Outlying Islands</option>
					<option value="VI">Virgin Islands</option>
					</optgroup>
					<optgroup label="Armed Forces">
					<option value="AA">Armed Forces Americas</option>
					<option value="AP">Armed Forces Pacific</option>
					<option value="AE">Armed Forces Others</option>
					</optgroup>& Territories"
					<optgroup label="Canadian Provinces & Territories">
					<option value="AB">Alberta</option>
					<option value="BC">British Columbia</option>
					<option value="MB">Manitoba</option>
					<option value="NB">New Brunswick</option>
					<option value="NL">Newfoundland and Labrador</option>
					<option value="NS">Nova Scotia</option>
					<option value="ON">Ontario</option>
					<option value="PE">Prince Edward Island</option>
					<option value="QC">Quebec</option>
					<option value="SK">Saskatchewan</option>
					<option value="NT">Northwest Territories</option>
					<option value="NU">Nunavut</option>
					<option value="YT">Yukon</option>
					</optgroup>
					<optgroup label="Mexican States">
					<option value="AGS">Aguascalientes</option>
					<option value="BCN">Baja California</option>
					<option value="BCS">Baja California Sur</option>
					<option value="CAM">Campeche</option>
					<option value="CHP">Chiapas</option>
					<option value="CHI">Chihuahua</option>
					<option value="COA">Coahuila</option>
					<option value="COL">Colima</option>
					<option value="DIF">Distrito Federal</option>
					<option value="DUR">Durango</option>
					<option value="MEX">Estado de M&eacute;xico</option>
					<option value="GTO">Guanajuato</option>
					<option value="GRO">Guerrero</option>
					<option value="HGO">Hidalgo</option>
					<option value="JAL">Jalisco</option>
					<option value="MIC">Michoac&aacute;n</option>
					<option value="MOR">Morelos</option>
					<option value="NAY">Nayarit</option>
					<option value="NLE">Nuevo Le&oacute;n</option>
					<option value="OAX">Oaxaca</option>
					<option value="PUE">Puebla</option>
					<option value="QUE">Quer&eacute;taro</option>
					<option value="ROO">Quintana Roo</option>
					<option value="SLP">San Luis Potos&iacute;</option>
					<option value="SIN">Sinaloa</option>
					<option value="SON">Sonora</option>
					<option value="TAB">Tabasco</option>
					<option value="TAM">Tamaulipas</option>
					<option value="TLA">Tlaxcala</option>
					<option value="VER">Veracruz</option>
					<option value="YUC">Yucat&aacute;n</option>
					<option value="ZAC">Zacatecas</option>
					</optgroup>
				</select>
			</div>
			<div class="third omega">
				<label for="zip">Zip:</label>
				<input type="text" id="zip" name="zip" class="input" />
			</div>
		</li>
		</fieldset>
		<fieldset><legend>In Case Of Emergency Please Notify</legend>
		<li>
			<div class="half">
			<label for="ICE_Name">Emergency Contact Name</label>
			<input name="ICE_Name" type="text" class="input"/>
			</div>
			<div class="half">
			<label for="ICE_Phone">Phone</label>
			<input name="ICE_Phone" type="text" class="input"/>
			</div>
		</li>
		<li>
			<div>
				<label for="ICE_Address">Address:</label>
				<input name="ICE_Address" type="text" class="input" />
			</div>
		</li>
		</fieldset>
		<fieldset><legend>Employment Overview</legend>
		<li>
			<div>
				<label for="PrevEmployedHere">Have You Ever Been Employed With J & B Staffing?</label>
				<input name="PrevEmployedHere" type="radio" value="Yes" /> Yes 
				<input name="PrevEmployedHere" type="radio" value="No" checked="checked" /> No
			</div>
		</li>
		<li>
			<div>
				<label for="JBCompaniesTemp">What Companies Did You Work For? </label>
				<textarea name="JBCompaniesTemp" rows="1" cols="19" style="width: 220px" /></textarea>
			</div>
		</li>
		<li>
			<div>
				<label for="Position">Position you are applying for *</label>
				<input name="Position" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="JB_Referred">How Were You Referred To J & B Staffing?</label>
				<input name="JB_Referred" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Days"><b>Which Days Are You Available To Work?</b></label>
				<input name="Availability_Days" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>	
<!--
		<li>
			<div>
				Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
				<label for="Availability_Days"><b>Which Days Are You Available To Work?</b></label>
				<input name="Availability_Days" type="text" maxlength="100" style="width: 220px" />
				<input name="Availability_Days[]" type="checkbox" value="Monday" /> Monday <br />  
				<input name="Availability_Days[]" type="checkbox" value="Tuesday" /> Tuesday <br />  
				<input name="Availability_Days[]" type="checkbox" value="Wednesday" /> Wednesday <br />  
				<input name="Availability_Days[]" type="checkbox" value="Thursday" /> Thursday <br />  
				<input name="Availability_Days[]" type="checkbox" value="Friday" /> Friday <br />  
				<input name="Availability_Days[]" type="checkbox" value="Saturday" /> Saturday <br />  
				<input name="Availability_Days[]" type="checkbox" value="Sunday" /> Sunday <br />  
			</div>
		</li>
-->
		<li>
			<div>
			<label for="Availability_Cities"><b>Cities You Are Available To Work In:</b></label>
			<input name="Availability_Cities" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
		<li>
			<div>
				<label for="Availability_Shifts"><b>Shifts Available: (1st, 2nd, AND/OR 3rd)</b></label></td>
				<input name="Availability_Shifts" type="text" maxlength="100" style="width: 220px" />
			</div>
		</li>
<!--
		<li>
			<div>
				<label for="Availability_Shifts"><b>Shifts Available:</b></label></td>
				<input name="Availability_Shifts[]" type="checkbox" value="1st" /> 1st  
				<input name="Availability_Shifts[]" type="checkbox" value="2nd" /> 2nd   
				<input name="Availability_Shifts[]" type="checkbox" value="3rd" /> 3rd 
			</div>
		</li>
-->
		<li>
			<div>
				<label for="Availability_Start"><b>When are you available to start?</b></label>
				<input name="Availability_Start" type="date" />
			</div>
		</li>
		<li>
			<div>
				<b>Please check any of the following that apply to you:</b> <br />
				<label name="Longterm">Available for long term assignments </label>	<input name="Longterm" type="radio" value="Yes" /> Yes
				<label name="Same_Day">Will accept same day assignments</label>	<input name="Same_Day" type="radio" value="Yes" /> Yes  
				<label name="Temp2Perm">Available for temporary to permanent assignments</label><input name="Temp2Perm" type="radio" value="Yes" /> Yes
				<label name="Car_Available">Car available </label><input name="Car_Available" type="radio" value="Yes" /> Yes
				<label for="OwnCar"> If yes, is this your own car? </label>
					<input name="OwnCar" type="radio" value="Yes"/> Yes 
					<input name="OwnCar" type="radio" value="No" /> No <br />
				<label name="Resume_Attached">Resume attached </label><input name="Resume_Attached" type="radio" value="Yes" /> Yes
				<label for="Resume" ><input name="Resume" type="file" /><br />
			</div>
		</li>
<!--		
		<li>
			<div>
				<label for="Availability_Other"><b>Please check any of the following that apply to you:</b></label>
				<input name="Availability_Other[]" type="checkbox" value="Longterm" /> Available for long term assignments <br />  
				<input name="Availability_Other[]" type="checkbox" value="Same_Day" /> Will accept same day assignments <br />  
				<input name="Availability_Other[]" type="checkbox" value="Temp2Perm" /> Available for temporary to permanent assignments <br /> 
				<input name="Availability_Other[]" type="checkbox" value="Car_Available" /> Car available 
					<label for="OwnCar"> If yes, is this your own car? </label> 
						<input name="OwnCar" type="radio" value="Yes"/> Yes
						<input name="OwnCar" type="radio" value="No" /> No <br />
				<input name="Availability_Other[]" type="checkbox" value="Resume" /> Resume attached <br />
				<label for="Resume" ><input name="Resume" type="file" /><br />
			</div>
		</li>
-->
		</fieldset>
		<fieldset><legend>Work Eligiblity</legend>
			<li>
				<div>
					<label for="Work_Eligiblity"><b>Are you legally eligible to work in the U.S?</b></label>
					<input name="Work_Eligiblity" type="radio" value="Yes" /> Yes      
					<input name="Work_Eligiblity" type="radio" value="No" checked="checked" /> No
				</div>
			</li>
			<li>
				<div>
				<label for="Majority"><b>Are you at least 18 years old?</b></label>
				<input name="Majority" type="radio" value="Yes" /> Yes      
				<input name="Majority" type="radio" value="No" checked="checked" /> No
				</div>
			</li>
			<li>
				<div>
				<label for="Felony"><b>Have You Ever Been Convicted Of A Felony?</b></label>
				<input name="Felony" type="radio" value="Yes" /> Yes      
				<input name="Felony" type="radio" value="No" /> No
				</div>
			</li>
			<li>
				<div>
				<label for="Felony_Explaination"><b>If Yes, Please Explain</b></label><br />
				<textarea name="Felony_Explaination" rows="7" cols="40" class="input"></textarea>
				</div>
			</li>
			</fieldset>
			<fieldset><legend>Education Background</legend>
			
			<table border="0" cellpadding="5" cellspacing="0">
			<tr> <td colspan="4"style="text-align: right;">Dates of Graduation</td></tr>
			<tr> <td colspan="2"><label for="High_School">High School Attended</label></td>
			<td>
			<input name="High_School" type="text" maxlength="50" style="width: 295px;" class="table" />
			</td><td><label for="High_School_GraduationDate"></label><input name="High_School_GraduationDate" type="date" class="table" /></td>
			<tr> <td colspan="2"><label for="College">College Attended</label></td>
			<td><input name="College" type="text" maxlength="50" style="width: 295px;" class="table"/></td>
			<td><label for="College_GraduationDate"></label><input name="College_GraduationDate" type="date" class="table" /></td> </tr>
			</table>
			<br />
			<li>
				<div>
				<label for="Degree_Emphasis">Degree/Emphasis</label><input name="Degree_Emphasis" type="text" class="input" />
				</div>
			</li>
			<li>
				<div>
				<label for="Special_Skills">Special Skills</label><input name="Special_Skills" type="text" class="input" />
				</div>
			</li>
			</fieldset>
			<fieldset><legend>Previous Employment History</legend>
			<!--	1st Employer	-->	

			<li>
				<div>
					<label for="1stEmployer_Name">Employer Name</label>
					<input name="1stEmployer_Name" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div>
					<label for="1stEmployer_Address">Address</label>
					<input name="1stEmployer_Address" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div class="half">
					<label for="1stEmployer_Supervisor">Name of Supervisor</label>
					<input name="1stEmployer_Supervisor" type="text" class="input"/>
				</div>
				<div class="half">
					<label for="1stEmployer_Phone">Phone Number</label>
					<input name="1stEmployer_Phone" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div class="half">
					<label for="1stEmployer_PayRate">Pay Rate</label>
					<input name="1stEmployer_PayRate" type="text" class="input" />
				</div>
				<div class="half">
					<label for="1stEmployer_DatesEmployed">Dates Employed</label>
					<input name="1stEmployer_DatesEmployed" type="text" class="input" />
				</div>
			</li>
			<li>
				<div>
				<label for="1stEmployer_Job">Job Title and Duties</label>
				<textarea name="1stEmployer_Job" rows="7" cols="40" class="input"></textarea>
				</div>
			</li>
			<li>
				<div>
					<label for="1stEmployer_Reason">Reason For Leaving</label>
					<input name="1stEmployer_Reason" type="text" class="input"/>
				</div>
			</li> 
			
			<br/>
				<!--	2nd Employer	-->	
			
			<li>
				<div>
					<label for="2ndEmployer_Name">Employer Name</label>
					<input name="2ndEmployer_Name" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div>
					<label for="2ndEmployer_Address">Address</label>
					<input name="2ndEmployer_Address" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div class="half">
					<label for="2ndEmployer_Supervisor">Name of Supervisor</label>
					<input name="2ndEmployer_Supervisor" type="text" class="input"/>
				</div>
				<div class="half">
					<label for="2ndEmployer_Phone">Phone Number</label>
					<input name="2ndEmployer_Phone" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div class="half">
					<label for="2ndEmployer_PayRate">Pay Rate</label>
					<input name="2ndEmployer_PayRate" type="text" class="input" />
				</div>
				<div class="half">
					<label for="2ndEmployer_DatesEmployed">Dates Employed</label>
					<input name="2ndEmployer_DatesEmployed" type="text" class="input" />
				</div>
			</li>
			<li>
				<div>
				<label for="2ndEmployer_Job">Job Title and Duties</label>
				<textarea name="2ndEmployer_Job" rows="7" cols="40" class="input"></textarea>
				</div>
			</li>
			<li>
				<div>
					<label for="2ndEmployer_Reason">Reason For Leaving</label>
					<input name="2ndEmployer_Reason" type="text" class="input"/>
				</div>
			</li> 
					
			<br/>
			<!--	3rd Employer	-->	
			
			<li>
				<div>
					<label for="3rdEmployer_Name">Employer Name</label>
					<input name="3rdEmployer_Name" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div>
					<label for="3rdEmployer_Address">Address</label>
					<input name="3rdEmployer_Address" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div class="half">
					<label for="3rdEmployer_Supervisor">Name of Supervisor</label>
					<input name="3rdEmployer_Supervisor" type="text" class="input"/>
				</div>
				<div class="half">
					<label for="3rdEmployer_Phone">Phone Number</label>
					<input name="3rdEmployer_Phone" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div class="half">
					<label for="3rdEmployer_PayRate">Pay Rate</label>
					<input name="3rdEmployer_PayRate" type="text" class="input" />
				</div>
				<div class="half">
					<label for="3rdEmployer_DatesEmployed">Dates Employed</label>
					<input name="3rdEmployer_DatesEmployed" type="text" class="input" />
				</div>
			</li>
			<li>
				<div>
				<label for="3rdEmployer_Job">Job Title and Duties</label>
				<textarea name="3rdEmployer_Job" rows="7" cols="40" class="input"></textarea>
				</div>
			</li>
			<li>
				<div>
					<label for="3rdEmployer_Reason">Reason For Leaving</label>
					<input name="3rdEmployer_Reason" type="text" class="input"/>
				</div>
			</li> 
			
			<br/>
			<!--	4th Employer	-->		
			
			<li>
				<div>
					<label for="4thEmployer_Name">Employer Name</label>
					<input name="4thEmployer_Name" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div>
					<label for="4thEmployer_Address">Address</label>
					<input name="4thEmployer_Address" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div class="half">
					<label for="4thEmployer_Supervisor">Name of Supervisor</label>
					<input name="4thEmployer_Supervisor" type="text" class="input"/>
				</div>
				<div class="half">
					<label for="4thEmployer_Phone">Phone Number</label>
					<input name="4thEmployer_Phone" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div class="half">
					<label for="4thEmployer_PayRate">Pay Rate</label>
					<input name="4thEmployer_PayRate" type="text" class="input" />
				</div>
				<div class="half">
					<label for="4thEmployer_DatesEmployed">Dates Employed</label>
					<input name="4thEmployer_DatesEmployed" type="text" class="input" />
				</div>
			</li>
			<li>
				<div>
				<label for="4thEmployer_Job">Job Title and Duties</label>
				<textarea name="4thEmployer_Job" rows="7" cols="40" class="input"></textarea>
				</div>
			</li>
			<li>
				<div>
					<label for="4thEmployer_Reason">Reason For Leaving</label>
					<input name="4thEmployer_Reason" type="text" class="input"/>
				</div>
			</li> 
			
			</fieldset>
		
			<fieldset><legend>References</legend>
			<!--	1st Reference	-->		
			<li>
				<div>
				<label for="1stReference_Name">Name</label>
				<input name="1stReference_Name" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div>
				<label for="1stReference_Address">Address</label>
				<input name="1stReference_Address" type="text" class="input" />
				</div>
			</li>
			<li>
				<div class="half">
				<label for="1stReference_Phone">Phone Number</label>
				<input name="1stReference_Phone" type="text" class="input" /></td> 
				</div>
				<div class="half">
				<label for="1stReference_Relation">Relationship</label>
				<input name="1stReference_Relation" type="text" class="input" />
				</div>
			</li>
			
			<br/>
			<!--	2nd Reference	-->		
			<li>
				<div>
				<label for="2ndReference_Name">Name</label>
				<input name="2ndReference_Name" type="text" class="input"/>
				</div>
			</li>
			<li>
				<div>
				<label for="2ndReference_Address">Address</label>
				<input name="2ndReference_Address" type="text" class="input" />
				</div>
			</li>
			<li>
				<div class="half">
				<label for="2ndReference_Phone">Phone Number</label>
				<input name="2ndReference_Phone" type="text" class="input" /></td> 
				</div>
				<div class="half">
				<label for="2ndReference_Relation">Relationship</label>
				<input name="2ndReference_Relation" type="text" class="input" />
				</div>
			</li>
			</fieldset>
			
			<fieldset><legend>Temporary Services</legend>
			<!--	1st Temporary Service	-->	
			<li>	
				<div>
				<label for="1stService_Name">Name of Service</label>
				<input name="1stService_Name" type="text" class="input" />
				</div>
			</li>
			<li>
				<div>
				<label for="1stService_Assigned_Companies">Name of Companies Assigned To</label>
				<textarea name="1stService_Assigned_Companies" rows="7" cols="40" class="input"></textarea>
				</div>
			</li>
			
			<br/>
			<!--	2nd Temporary Service	-->	
			
			<li>
				<div>
				<label for="2ndService_Name">Name of Service</label>
				<input name="2ndService_Name" type="text" class="input" />
				</div>
			</li>
			<li>
				<div>
				<label for="2ndService_Assigned_Companies">Name of Companies Assigned To</label>
				<textarea name="2ndService_Assigned_Companies" rows="7" cols="40" class="input"></textarea>
				</div>
			</li>
			
			<br/>
			<!--	3rd Temporary Service	-->	
			
			<li>
				<div>
				<label for="3rdService_Name">Name of Service</label>
				<input name="3rdService_Name" type="text" class="input" />
				</div>
			</li>
			<li>
				<div>
				<label for="3rdService_Assigned_Companies">Name of Companies Assigned To</label>
				<textarea name="3rdService_Assigned_Companies" rows="7" cols="40" class="input"></textarea>
				</div>
			</li>
			</fieldset>
			

			<fieldset><legend>CERTIFICATION & SUMMITTION</legend>
			<li>
				<div>
				PLEASE READ CERTIFICATION THOROUGHLY:
				</div>
			</li>

			<li>
				<div>
				If employed, I agree that if at any time I shall make claims against the company for personal injuries, upon written request I will submit myself to examination by a physician or physicians of the company&rsquo;s selection (at the company&rsquo;s expense) as often as may be requested. I certify that the facts contained in this application and interview are true and complete. I understand that, if employed, falsified statements shall be grounds for dismissal. I understand and agree that, if hired, my employment is for no definite period and may be terminated at any time without prior notice. I authorize J&B Staffing to contact my previous employers and educational institutions regarding my employment and education.
				</div>
			</li>

			<br /><br />

			<li>
				<div>
				<input type="checkbox" name="readcert" required>I HAVE CAREFULLY READ THE ABOVE CERTIFICATION AND I UNDERSTAND AND AGREE TO ITS TERMS.
				</div>
			</li>

			<br /><br />

			<li>
				<div class="half">
				<label for="Signature">Name</label><input name="Signature" type="text" class="input" />
				</div>
			
				<div class="half">
				<label for="Signature_Date">Date</label><input name="Signature_Date" type="date" class="input" />
				</div>
			</li>

		</ul>
		<p>
		<input type="submit" name="submit" value="Send" />
		</p>
	</form>
<?php include("includes/overall/footer.php"); ?>