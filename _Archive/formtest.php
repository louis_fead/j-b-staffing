<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Add Records Form</title>
</head>
<body>
<form action="phptest.php" method="post">
	<p>
    	<label for="firstName">First Name:</label>
        <input type="text" name="firstname" id="firstName">
    </p>
    <p>
    	<label for="lastName">Last Name:</label>
        <input type="text" name="lastname" id="lastName">
    </p>
    <p>
    	<label for="emailAddress">Email Address:</label>
        <input type="text" name="email" id="emailAddress">
    </p>
	<p>
    	<label for="Phone">Phone</label>
        <input type="tel" name="phone" id="Phone">
    </p>
	<p>
    	<label for="Street">Street:</label>
        <input type="text" name="street" id="Street">
    </p>
	<p>
    	<label for="City">City:</label>
        <input type="text" name="city" id="City">
    </p>
	<p>
    	<label for="Position">Position:</label>
        <input type="text" name="position" id="Position">
    </p>
    <input type="submit" value="Add Records">
</form>
</body>
</html>